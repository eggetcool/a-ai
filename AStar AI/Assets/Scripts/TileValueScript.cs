﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class TileValueScript : MonoBehaviour {
    
    private bool gameLoop;
    private bool btndwn;
    private int tmp;

    [SerializeField]
    private List<GameObject> openNodes      = new List<GameObject>();
    private List<GameObject> closedNodes    = new List<GameObject>();
    private List<GameObject> tiles          = new List<GameObject>();
    private List<GameObject> shortestPath   = new List<GameObject>();

    [SerializeField]
    private GameObject curNode;
    private GameObject startNode;
    private GameObject endNode;
    private Vector2 startPos;
    private Vector2 endPos;

    // Use this for initialization
    void Start ()
    {
        initProg();
    }
	
    void initProg()
    {
        openNodes.Clear();
        closedNodes.Clear();
        
        gameLoop   = true;
        tmp        = 0;
        tiles      = GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().m_Tiles;
        startPos   = GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().StartPos;
        endPos     = GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().EndPos;
        startNode  = tiles[(int)(startPos.x + (GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().numberOfTiles * startPos.y))];
        endNode    = tiles[(int)(endPos.x + (GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().numberOfTiles * endPos.y))];


        for (int i = 0; i < openNodes.Count; i++)
        {
            openNodes[i].GetComponent<SpriteRenderer>().color = Color.white;
        }
        for (int i = 0; i < closedNodes.Count; i++)
        {
            closedNodes[i].GetComponent<SpriteRenderer>().color = Color.white;
        }

        openNodes.Add(startNode);
        curNode = startNode;
    }

	// Update is called once per frame
	void Update ()
    {

        if (Input.GetMouseButtonDown(1))
            btndwn = !btndwn;

        if (openNodes.Count > 0 && gameLoop == true && btndwn)
            PathFinding();

        if(openNodes.Count <= 0)
        {
            Debug.Log("No Path");

            if (!btndwn)
                SceneManager.LoadScene("GameScene");
        }

        if (gameLoop == false)
        {
            shortestPath = retracePath(startNode, endNode);

            GameObject.FindGameObjectWithTag("startnode").GetComponent<MoveScript>().Go = true;

            if (!btndwn)
                SceneManager.LoadScene("GameScene");
        }
    }

    void PathFinding()
    {
        curNode = null;

        for (int i = 0; i < openNodes.Count; i++)
        {
            if (curNode == null)
            {
                curNode = openNodes[i];
                continue;
            }

            if (openNodes[i].GetComponent<IndividualTileValues>().FCost <= curNode.GetComponent<IndividualTileValues>().FCost &&
                openNodes[i].GetComponent<IndividualTileValues>().HCost < curNode.GetComponent<IndividualTileValues>().HCost)
            {
                curNode = openNodes[i];
            }
        }
        
        openNodes.Remove(curNode);
        closedNodes.Add(curNode);
        curNode.GetComponent<SpriteRenderer>().color = Color.red;


        if ((Vector2)curNode.transform.position == endPos)
        {
            Debug.Log("Path Found!");
            gameLoop = false;
        }
        else
        {
            ExploreNodes(curNode.transform.position);

            if(TilePlacementScript.SearchInstant)
                PathFinding();
        }
    }

    void ExploreNodes(Vector2 pos)
    {
        //Kan optimiseras...
        for (int i = 0; i < tiles.Count; i++)
        {
            if(tiles[i].transform.position.x == Mathf.Clamp(tiles[i].transform.position.x, pos.x - 1, pos.x + 1) && 
               tiles[i].transform.position.y == Mathf.Clamp(tiles[i].transform.position.y, pos.y - 1, pos.y + 1))
            {
                if (closedNodes.Contains(tiles[i]) || tiles[i].GetComponent<IndividualTileValues>().setWall == true)
                    continue;
                
                int NewCostToNeighbour = (int)(curNode.GetComponent<IndividualTileValues>().GCost) + Mathf.RoundToInt(Vector2.Distance(curNode.transform.position, tiles[i].transform.position) * 10);
                if ((NewCostToNeighbour < tiles[i].GetComponent<IndividualTileValues>().GCost) || !openNodes.Contains(tiles[i]))
                {
                    tiles[i].GetComponent<SpriteRenderer>().color = Color.gray;
                    tiles[i].GetComponent<IndividualTileValues>().GCost = NewCostToNeighbour;
                    tiles[i].GetComponent<IndividualTileValues>().FCost = NodeCalc.NodeCostCalc(tiles[i], tiles[i].GetComponent<IndividualTileValues>().GCost);
                    tiles[i].GetComponent<IndividualTileValues>().HCost = NodeCalc.hCost;
                    
                    tiles[i].transform.SetParent(curNode.transform);

                    if (!openNodes.Contains(tiles[i]))
                        openNodes.Add(tiles[i]);
                }
            }
        }
    }

    private List<GameObject> retracePath(GameObject start, GameObject end)
    {
        List<GameObject> path = new List<GameObject>();
        GameObject currentNode = end;

        while (currentNode != start)
        {
            currentNode.GetComponent<SpriteRenderer>().color = Color.green;
            path.Add(currentNode);
            currentNode = currentNode.transform.parent.gameObject;
        }

        path.Reverse();

        return path;
    }

    public List<GameObject> GetPath
    {
        get { return shortestPath; }
    }
}
