﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour {

    private bool go;
    private bool play;
    private int index;
    private Vector2 startPos;
    private GameObject gameController;
    private List<GameObject> path = new List<GameObject>();
    private AudioSource audio = new AudioSource();

	// Use this for initialization
	void Start () {
        index = 1;
        go = false;
        play = true;
        startPos = transform.position;
        gameController = GameObject.FindGameObjectWithTag("GameController");
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		if(go)
        {
            audio.pitch = (startPos.x - transform.position.x) / 6;
            path = gameController.transform.GetComponent<TileValueScript>().GetPath;
            walkTowardsGoal();
        }
	}

    void walkTowardsGoal()
    {
        if(transform.position != path[path.Count - 1].transform.position)
        {
            if(play)
            {
                play = false;
                audio.Play();
            }
            transform.position = Vector2.MoveTowards(transform.position, path[index].transform.position, Time.deltaTime * 10);
            if (transform.position == path[index].transform.position)
                index++;
        }
        else
        {
            audio.Stop();
        }
    }

    public bool Go
    {
        set { go = value; }
    }
}
