﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeCalc : MonoBehaviour {
    public static Vector2 endPos;
    public static Vector2 startPos;

    [HideInInspector]
    public static float hCost;

    private static float fCost;


    public static float NodeCostCalc(GameObject tile, float gCost)
    {
        float dx = Mathf.Abs(endPos.x - tile.transform.position.x);
        float dy = Mathf.Abs(endPos.y - tile.transform.position.y);
        hCost = Mathf.Min(dx, dy) * 14 + Mathf.Abs(dx - dy) * 10;

        fCost = gCost + hCost;
        //Debug.Log("Tile " + tile.transform.position + ": Gcost: " + gCost + " Hcost: " + hCost + " Fcost: " + fCost);
        return fCost;
    }
}
