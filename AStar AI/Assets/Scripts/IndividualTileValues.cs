﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndividualTileValues : MonoBehaviour {
    [SerializeField]
    private float fCost;
    private float gCost;
    private float hCost;

    public bool isWall;


	// Use this for initialization
	void Start () {
        isWall = false;
        gCost = 0;
    }

    public float FCost
    {
        get { return fCost; }
        set { fCost = value; }
    }

    public float HCost
    {
        get { return hCost; }
        set { hCost = value; }
    }

    public float GCost
    {
        get { return gCost; }
        set { gCost = value; }
    }

    public bool setWall
    {
        get { return isWall; }
        set { isWall = value;
            fCost = 10000;
            gCost = 10000;
            hCost = 10000; }
    }
}
