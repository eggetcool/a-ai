﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TilePlacementScript : MonoBehaviour {
    public int numberOfTiles;
    public static bool SearchInstant;
    private Vector2 startPos;
    private Vector2 endPos;
    private Vector3 mousePos;

    [HideInInspector]
    public List<GameObject> m_Tiles = new List<GameObject>();
    private List<GameObject> m_Walls = new List<GameObject>();
    private GameObject m_Start;
    private GameObject m_End;

    // Use this for initialization
    void Start ()
    {
        //SearchInstant = false;
        StartPos = new Vector2(Random.Range(0, numberOfTiles - 1), Random.Range(0, numberOfTiles - 1));
        EndPos = new Vector2(Random.Range(0, numberOfTiles - 1), Random.Range(0, numberOfTiles - 1));

        m_Start = Instantiate(Resources.Load("startnode", typeof(GameObject))) as GameObject; 
        m_Start.transform.position = StartPos;
        NodeCalc.startPos = StartPos;

        m_End = Instantiate(Resources.Load("endnode", typeof(GameObject))) as GameObject;
        m_End.transform.position = EndPos;
        NodeCalc.endPos = EndPos;

        int tmp = 0;
        for (int i = 0; i < numberOfTiles; i++)
        {
            for (int j = 0; j < numberOfTiles; j++)
            {
                m_Tiles.Add(Instantiate(Resources.Load("tile", typeof(GameObject))) as GameObject);
                m_Tiles[tmp].transform.position = new Vector3(j, i, 0);
                tmp++;
                
            }
        }

        m_Tiles[(int)(StartPos.x + (GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().numberOfTiles * StartPos.y))].
            GetComponent<IndividualTileValues>().FCost = NodeCalc.NodeCostCalc(m_Start, 0.0f);
        m_Tiles[(int)(StartPos.x + (GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().numberOfTiles * StartPos.y))].
            GetComponent<IndividualTileValues>().HCost = NodeCalc.hCost;
    }
	
	// Update is called once per frame
	void Update ()
    {
        
        if (Input.GetMouseButton(0))
        {
            mousePos = Input.mousePosition;

            //Jesus christ  monobehaviour....
            m_Tiles[Mathf.RoundToInt(Camera.main.ScreenToWorldPoint(mousePos).x) + (GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().numberOfTiles * Mathf.RoundToInt(Camera.main.ScreenToWorldPoint(mousePos).y))]
                .GetComponent<SpriteRenderer>().color = Color.black;

            m_Tiles[Mathf.RoundToInt(Camera.main.ScreenToWorldPoint(mousePos).x) + (GameObject.FindGameObjectWithTag("GameController").transform.GetComponent<TilePlacementScript>().numberOfTiles * Mathf.RoundToInt(Camera.main.ScreenToWorldPoint(mousePos).y))]
                .GetComponent<IndividualTileValues>().setWall = true;
        }
    }

    public Vector2 StartPos
    {
        get { return startPos; }
        set { startPos = value; }
    }

    public Vector2 EndPos
    {
        get { return endPos; }
        set { endPos = value; }
    }

    public void searchInstantFalse()
    {
        SearchInstant = !SearchInstant;
    }
}
