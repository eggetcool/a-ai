﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class buttonspritescript : MonoBehaviour {

    Image colorOnButton;

	// Use this for initialization
	void Start () {
        colorOnButton = GetComponent<Image>();

    }
	
	// Update is called once per frame
	void Update () {

        if (TilePlacementScript.SearchInstant)
            colorOnButton.color = Color.green;
        else
            colorOnButton.color = Color.white;

    }
}
